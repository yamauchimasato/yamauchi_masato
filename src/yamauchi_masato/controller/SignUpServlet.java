package yamauchi_masato.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils; //文字列nullでも例外でない

import yamauchi_masato.beans.User;
import yamauchi_masato.service.UserService;

@WebServlet(urlPatterns = { "/signup"}) //top画面から受けとるタグ
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		    	List<String> messages = new ArrayList<String>();

		    	HttpSession session = request.getSession();
		        if (isValid(request, messages) == true) {
		        	User user = new User();
		        	user.setLoginId(Integer.parseInt(request.getParameter("login_id")));
		        	user.setName(request.getParameter("name"));
		        	user.setPassword(request.getParameter("password1"));
		        	user.setBranch_office(Integer.parseInt(request.getParameter("branch_office")));
		        	user.setPosition(Integer.parseInt(request.getParameter("position")));
		        	new UserService().register(user);
		        	response.sendRedirect("./");
		    	} else {
		    		session.setAttribute("errorMesasges", messages);
		    		response.sendRedirect("signup");
		    	}
	}

    //バリデーション
    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String loginId = request.getParameter("login_id");
    	String name = request.getParameter("name");
    	String password = request.getParameter("password1");
    	String branch_office = request.getParameter("branch_office");
    	String position = request.getParameter("position");

    	if(StringUtils.isEmpty(loginId)) {
    		messages.add("ログインIDを入力してください");
    	}
    	if(StringUtils.isEmpty(password)) {
    		messages.add("氏名を入力してください");
    	}
    	if(StringUtils.isEmpty(name)) {
    		messages.add("パスワードを入力してください");
    	}
    	if(StringUtils.isEmpty(branch_office)) {
    		messages.add("支店コードを入力してください");
    	}
    	if(StringUtils.isEmpty(position)) {
    		messages.add("部署・役職コードを入力してください");
    	}

    	//すでに登録済みのとき
    	if(messages.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
}

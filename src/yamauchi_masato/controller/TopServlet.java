package yamauchi_masato.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yamauchi_masato.beans.Comment;
import yamauchi_masato.beans.UserMessage;
import yamauchi_masato.service.CommentService;
import yamauchi_masato.service.MessageService;
import yamauchi_masato.service.SerchService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<UserMessage> messages = null;
		List<Comment> comments = null;

		//カテゴリ検索時、部分一致検索して投稿一覧表示
		if (StringUtils.isNotEmpty(request.getParameter("categorySerch"))){

			String categorySerch = request.getParameter("categorySerch");
			messages = new SerchService().categorySerch(categorySerch);
			comments = new CommentService().getComment();

		//日付指定時、投稿日時検索して投稿一覧表示
		}
		else if(StringUtils.isNotEmpty(request.getParameter("dateSerch"))) {

			String dateSerch = request.getParameter("dateSerch");
			messages = new SerchService().dateSerch(dateSerch);
			comments = new CommentService().getComment();

		//検索ボックス空の時、通常の投稿一覧表示
		} else {

			messages = new MessageService().getMessage();
			comments = new CommentService().getComment();

		}

		session.getAttribute("loginUser");
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}



}

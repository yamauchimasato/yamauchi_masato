package yamauchi_masato.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yamauchi_masato.beans.Message;
import yamauchi_masato.beans.User;
import yamauchi_masato.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if(isValid(request,messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("message"));
			message.setLoginId(user.getLoginId());
			message.setCreatedDate(user.getCreatedDate());

			new MessageService().register(message);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User)request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if(user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/message.jsp").forward(request, response);
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("message");

		if(StringUtils.isEmpty(message) == true) {
			messages.add("メッセージを入力してください");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

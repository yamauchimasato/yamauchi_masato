package yamauchi_masato.service;

import static yamauchi_masato.utils.CloseableUtil.*;
import static yamauchi_masato.utils.DBUtil.*;

import java.sql.Connection;

import yamauchi_masato.beans.User;
import yamauchi_masato.dao.UserDao;
import yamauchi_masato.utils.CipherUtil;

public class LoginService {

	public User login(int login_Id, String password) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, login_Id, encPassword);

			commit(connection);

			return user;
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
	}
}

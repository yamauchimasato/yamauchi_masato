package yamauchi_masato.service;

import static yamauchi_masato.utils.CloseableUtil.*;
import static yamauchi_masato.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import yamauchi_masato.beans.UserMessage;
import yamauchi_masato.dao.SerchDao;

public class SerchService {
	public List<UserMessage> categorySerch(String categorySerch) {
		Connection connection = null;
		try {
			connection = getConnection();
			SerchDao serchDao = new SerchDao();
			List<UserMessage> ret = serchDao.getCategorySerchMessages(connection, categorySerch);
			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> dateSerch(String dateSerch) {
		Connection connection = null;
		try {
			connection = getConnection();
			SerchDao serchDao = new SerchDao();
			List<UserMessage> ret = serchDao.getDateSerchMessages(connection, dateSerch);
			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}

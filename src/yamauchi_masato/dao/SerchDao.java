package yamauchi_masato.dao;

import static yamauchi_masato.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import yamauchi_masato.beans.UserMessage;
import yamauchi_masato.exception.SQLRuntimeException;

public class SerchDao {

	//カテゴリ検索のSQL
	public List<UserMessage> getCategorySerchMessages(Connection connection, String categorySerch) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id AS id,");
			sql.append("posts.title AS title,");
			sql.append("posts.text AS text,");
			sql.append("posts.category AS category,");
			sql.append("posts.login_id AS login_id,");
			sql.append("users.name AS name,");
			sql.append("posts.created_date AS created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
            sql.append("ON posts.login_id = users.login_id ");
            sql.append("WHERE category ");
            sql.append("LIKE ");
            sql.append("?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + categorySerch + "%");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toCommentList(rs);

			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//日付検索のSQL
	public List<UserMessage> getDateSerchMessages(Connection connection, String dateSerch) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id AS id,");
			sql.append("posts.title AS title,");
			sql.append("posts.text AS text,");
			sql.append("posts.category AS category,");
			sql.append("posts.login_id AS login_id,");
			sql.append("users.name AS name,");
			sql.append("posts.created_date AS created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
            sql.append("ON posts.login_id = users.login_id ");
            sql.append("WHERE posts.created_date ");
            sql.append("LIKE ");
            sql.append("?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, dateSerch + "%");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toCommentList(rs);

			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toCommentList(ResultSet rs)
		throws SQLException {
		List<UserMessage> ret = new ArrayList<UserMessage>();

		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String text = rs.getString("text");

				String category = rs.getString("category");
				int loginId = rs.getInt("login_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage userMessage = new UserMessage();
				userMessage.setId(id);
				userMessage.setName(name);
				userMessage.setTitle(title);
				userMessage.setText(text);
				userMessage.setCategory(category);
				userMessage.setLoginId(loginId);
				userMessage.setCreatedDate(createdDate);

				ret.add(userMessage);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}

package yamauchi_masato.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int loginId;
	private String name;
	private String password;
	private int branch_office;
	private int position;
	private String branchOfficeName;
	private String positionName;
    private Date createdDate;
    private Date updatedDate;

	//ゲッターとセッターを定義
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getBranch_office() {
		return branch_office;
	}
	public void setBranch_office(int branch_office) {
		this.branch_office = branch_office;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
    public Date getCreatedDate() {
    	return this.createdDate;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
    public Date getUpdatedDate() {
    	return this.updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = createdDate;
    }

	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	public String getBranchOfficeName() {
		return branchOfficeName;
	}
	public void setBranchOfficeName(String branchOfficeName) {
		this.branchOfficeName = branchOfficeName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
}

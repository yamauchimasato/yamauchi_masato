<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
					<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="post">
				<br /> <label for="login_id">ログインID</label> <input name="login_id" id="login_id" /> <br />
				<label for="password">パスワード</label> <input name="password1" type="password" id="password" /><br />
				<label for="password">確認用パスワード</label> <input name="password2" type="password" id="password_second" />
				(確認の為にもう一度パスワードを入力してください)<br />
				<label for="name">氏名</label> <input name="name" id="name" /> <br />
				<label for="branch_office">支店名</label>
					<select name="branch_office">
						<option value="1">本社</option>
						<option value="2">支店A</option>
						<option value="3">支店B</option>
						<option value="4">支店C</option>
					</select> <br />
				<label for="position">部署・役職名</label>
					<select name="position">
						<option value="1">総務人事担当者</option>
						<option value="2">情報管理担当者</option>
						<option value="3">支店長</option>
						<option value="4">社員</option>
					</select><br />
				<input type="submit" value="登録" /> <br /> <a href="./">ホームへ戻る</a>
			</form>
			<div class="copyright">Copyright(c)Yamauchi</div>
		</div>
	</body>
</html>
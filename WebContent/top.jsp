<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板システム</title>
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					☆<a href="newMessage">新規投稿</a><br />
					☆<a href="adminSettings">ユーザー管理</a>
				</c:if>
			</div>

			<c:if test="${ not empty loginUser }">
				<div class="messages">
					<form method="get"><br />
						<label for="categorySerch">カテゴリで検索</label>
						<input type="text" name="categorySerch" />  <input type="submit" value="検索" /><br />
						<label for="dateSerch"> 投稿日で検索</label>
						<input type="date" name="dateSerch" /> <input type="submit" value="検索" /><br />
					</form><br />

					<c:forEach items="${messages}" var="message">
						<c:set value="${loginUser}" var="loginUser" />
							<div class="message">
								<div class="name">ユーザー:<c:out value="${message.name}" />さん</div>
								<div class="title">件名: <c:out value="${message.title}" /> </div>
								<div class="category">カテゴリ: <c:out value="${message.category}" /> </div>
								<div class="text">本文: <c:out value="${message.text}" /> </div>
								(<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />)<br />
									<c:if test="${loginUser.loginId == message.loginId}">
										<form action="delete" method="post">
											<input type="hidden" name="postDelete" value="${message.id}">
											<input type="submit" value="削除" />
										</form>
									</c:if>
							</div><br />
							<div class="comments">
								<c:forEach items="${comments}" var="comment">
									<c:if test="${message.id == comment.messageId}">
										<div class="userId"><c:out value="${comment.userId}" />さんからのコメント</div>
										<div class="text"><c:out value="${comment.text}" /></div>
										<div class="createdDate">(<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />)</div>
											<c:if test="${loginUser.id == comment.userId}">
												<form action="delete" method="post">
													<input type="hidden" name="commentDelete" value="${comment.id}">
													<input type="submit" value="削除" />
												</form>
											</c:if>
									</c:if>
								</c:forEach>
							</div>
							<div class="comment-area">
								<form action="newComment" method="post"><br />
									<textarea  name="comment" rows="5" cols="60" class="comment-box"></textarea>
									<br />
									<input type="hidden" name="messageId" value="${message.id}">
									<input type="submit" value="コメント"> (500文字まで)
								</form><br />
							</div>
					</c:forEach>

				</div>
				<br />
			</c:if>
			<div class="copylight"> Copyright(c)Yamauchi</div>
		</div>
	</body>
</html>
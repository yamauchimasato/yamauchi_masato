<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
	</head>
	<body>
		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="newMessage" method="post">
					<br /><u><font size="4">件名</font></u>
					<br /><textarea name="title" cols="40" rows="2" class="text-box"></textarea>
					<br /><u><font size="4">カテゴリ</font></u>
					<br /><textarea name="category" cols="40" rows="2" class="text-box"></textarea>
					<br /><u><font size="4">本文</font></u>
					<br /><textarea name="message" cols="100" rows="12" class="text-box"></textarea>
					<br />
				<input type="submit" value="投稿"> <br /> <a href="./"><br />ホームへ戻る</a>
				</form>
			</c:if>
			<br />
			<div class="copylight"> Copyright(c)Yamauchi</div>
		</div>
	</body>
</html>